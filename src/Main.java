import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class Main {


    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static Empleado[] empleados = new Empleado[5];
    static int pos = 0;

    public static void main(String args[]) throws java.io.IOException
    {
        int opcion = -1;
        do {
            opcion = mostrarMenu();
            procesarOpcion(opcion);
        } while (opcion != 0);

    }
    static void procesarOpcion(int pOpcion) throws java.io.IOException
    {
        switch (pOpcion)
        {
            case 1:
                registrarEmpleado();
                break;
            case 2:
                listarEmpleados();
                break;
            case 0:
                out.println("Fin del programa !!");
                break;
            default:
                out.println("Opcion inválida");
        }
    }
    static int mostrarMenu() throws java.io.IOException
    {
        int opcion;
        opcion = -1;
        out.println("1. Registrar Empleado");
        out.println("2. Listar empleados");
        out.println("0. Salir del sistema");
        out.println("Digite la opción");
        opcion = Integer.parseInt(in.readLine());
        return opcion;
    }
    static void registrarEmpleado() throws IOException {
        out.println("Digite el nombre del empleado");
        String nombre = in.readLine();
        Empleado tmpEmpleado = new Empleado();//INICIAIZANDO EL OBJETO
        tmpEmpleado.setNombre(nombre);
    }
    static void listarEmpleados(){
        out.println("-- LISTA DE EMPLEADOS");
        for(int i=0;i<pos;i++){
            out.println(empleados[i].getNombre());
        }

    }
}
